:experimental:

== Actions reference
Below is a list of every available *action* in Pcbnew: a command that can be assigned to a hotkey.
Hotkeys that are shared between all KiCad applications are listed in the KiCad manual and are not
included here.

////
Note to translators: you do not need to translate this table by hand.

It is generated from KiCad using the Dump Hotkeys button that is shown in the hotkeys editor if you
add the line `HotkeysDumper=1` to your advanced config file (`kicad_advanced` file in the config
directory)
////

=== PCB Editor

[width="100%",options="header",cols="25%,15%,60%"]
|===
| Action | Default Hotkey | Description
| Align to Bottom
  |
  | Aligns selected items to the bottom edge
| Align to Vertical Center
  |
  | Aligns selected items to the vertical center
| Align to Horizontal Center
  |
  | Aligns selected items to the horizontal center
| Align to Left
  |
  | Aligns selected items to the left edge
| Align to Right
  |
  | Aligns selected items to the right edge
| Align to Top
  |
  | Aligns selected items to the top edge
| Distribute Horizontally
  |
  | Distributes selected items along the horizontal axis
| Distribute Vertically
  |
  | Distributes selected items along the vertical axis
| Place Off-Board Footprints
  |
  | Performs automatic placement of components outside board area
| Place Selected Footprints
  |
  | Performs automatic placement of selected components
| Flip Board View
  |
  | View board from the opposite side
| Sketch Graphic Items
  |
  | Show graphic items in outline mode
| Decrease Layer Opacity
  | kbd:[{]
  | Make the current layer more transparent
| Increase Layer Opacity
  | kbd:[}]
  | Make the current layer less transparent
| Switch to Component (F.Cu) layer
  | kbd:[PgUp]
  | 
| Switch to Inner layer 1
  |
  | 
| Switch to Inner layer 2
  |
  | 
| Switch to Inner layer 3
  |
  | 
| Switch to Inner layer 4
  |
  | 
| Switch to Inner layer 5
  |
  | 
| Switch to Inner layer 6
  |
  | 
| Switch to Inner layer 7
  |
  | 
| Switch to Inner layer 8
  |
  | 
| Switch to Inner layer 9
  |
  | 
| Switch to Inner layer 10
  |
  | 
| Switch to Inner layer 11
  |
  | 
| Switch to Inner layer 12
  |
  | 
| Switch to Inner layer 13
  |
  | 
| Switch to Inner layer 14
  |
  | 
| Switch to Inner layer 15
  |
  | 
| Switch to Inner layer 16
  |
  | 
| Switch to Inner layer 17
  |
  | 
| Switch to Inner layer 18
  |
  | 
| Switch to Inner layer 19
  |
  | 
| Switch to Inner layer 20
  |
  | 
| Switch to Inner layer 21
  |
  | 
| Switch to Inner layer 22
  |
  | 
| Switch to Inner layer 23
  |
  | 
| Switch to Inner layer 24
  |
  | 
| Switch to Inner layer 25
  |
  | 
| Switch to Inner layer 26
  |
  | 
| Switch to Inner layer 27
  |
  | 
| Switch to Inner layer 28
  |
  | 
| Switch to Inner layer 29
  |
  | 
| Switch to Inner layer 30
  |
  | 
| Switch to Copper (B.Cu) layer
  | kbd:[PgDn]
  | 
| Switch to Next Layer
  | kbd:[+]
  | 
| Switch to Previous Layer
  | kbd:[-]
  | 
| Toggle Layer
  | kbd:[V]
  | Switch between layers in active layer pair
| Net Inspector
  |
  | Show the net inspector
| Highlight Ratsnest
  |
  | Show ratsnest of selected item(s)
| Sketch Pads
  |
  | Show pads in outline mode
| Curved Ratsnest Lines
  |
  | Show ratsnest with curved lines
| Repair Board
  |
  | Run various diagnostics and attempt to repair board
| Show Appearance Manager
  |
  | Show/hide the appearance manager
| Show pad numbers
  |
  | Show pad numbers
| Scripting Console
  |
  | Show the Python scripting console
| Show Ratsnest
  |
  | Show board ratsnest
| Sketch Text Items
  |
  | Show footprint texts in line mode
| Sketch Tracks
  | kbd:[K]
  | Show tracks in outline mode
| Sketch Vias
  |
  | Show vias in outline mode
| Wireframe Zones
  |
  | Show only zone boundaries
| Fill Zones
  |
  | Show filled areas of zones
| Sketch Zones
  |
  | Show solid areas of zones in outline mode
| Toggle Zone Display
  | kbd:[A]
  | Cycle between showing filled zones, wireframed zones and sketched zones
| Automatically zoom to fit
  |
  | Zoom to fit when changing footprint
| Convert to Arc
  |
  | Converts selected line segment to an arc
| Convert to Rule Area
  |
  | Creates a rule area from the selection
| Convert to Lines
  |
  | Creates graphic lines from the selection
| Convert to Polygon
  |
  | Creates a graphic polygon from the selection
| Convert to Tracks
  |
  | Converts selected graphic lines to tracks
| Convert to Zone
  |
  | Creates a copper zone from the selection
| Design Rules Checker
  |
  | Show the design rules checker window
| Open in Footprint Editor
  | kbd:[Ctrl+E]
  | Opens the selected footprint in the Footprint Editor
| Append Board...
  |
  | Open another board and append its contents to this board
| Board Setup...
  |
  | Edit board setup including layers, design rules and various defaults
| Clear Net Highlighting
  | kbd:[~]
  | Clear any existing net highlighting
| Drill/Place File Origin
  |
  | Place origin point for drill files and component placement files
| Export Specctra DSN...
  |
  | Export Specctra DSN routing info
| BOM...
  |
  | Create bill of materials from board
| IPC-D-356 Netlist File…
  |
  | Generate IPC-D-356 netlist file
| Drill Files (.drl)...
  |
  | Generate Excellon drill file(s)
| Gerbers (.gbr)...
  |
  | Generate Gerbers for fabrication
| Component Placement (.pos)...
  |
  | Generate component placement file(s) for pick and place
| Footprint Report (.rpt)...
  |
  | Create report of all footprints from current board
| Group
  |
  | Group the selected items so that they are treated as a single item
| Enter Group
  |
  | Enter the group to edit items
| Leave Group
  |
  | Leave the current group
| Hide Net
  |
  | Hide the ratsnest for the selected net
| Highlight Net
  |
  | Highlight the selected net
| Highlight Net
  | kbd:[`]
  | Highlight all copper items on the selected net(s)
| Import Netlist...
  |
  | Read netlist and update board connectivity
| Import Specctra Session...
  |
  | Import routed Specctra session (*.ses) file
| Lock
  |
  | Prevent items from being moved and/or resized on the canvas
| Add Footprint
  | kbd:[O]
  | Add a footprint
| Add Layer Alignment Target
  |
  | Add a layer alignment target
| Remove Items
  |
  | Remove items from group
| Switch to Schematic Editor
  |
  | Open schematic in Eeschema
| Show Net
  |
  | Show the ratsnest for the selected net
| Toggle Last Net Highlight
  |
  | Toggle between last two highlighted nets
| Toggle Lock
  | kbd:[L]
  | Lock or unlock selected items
| Toggle Net Highlight
  | kbd:[Ctrl+`]
  | Toggle net highlighting
| Switch Track Width to Previous
  | kbd:[Shift+W]
  | Change track width to previous pre-defined size
| Switch Track Width to Next
  | kbd:[W]
  | Change track width to next pre-defined size
| Ungroup
  |
  | Ungroup any selected groups
| Unlock
  |
  | Allow items to be moved and/or resized on the canvas
| Decrease Via Size
  | kbd:[\ ]
  | Change via size to previous pre-defined size
| Increase Via Size
  | kbd:[']
  | Change via size to next pre-defined size
| Duplicate Zone onto Layer…
  |
  | Duplicate zone outline onto a different layer
| Merge Zones
  |
  | Merge zones
| Change Footprint…
  |
  | Assign a different footprint from the library
| Change Footprints...
  |
  | Assign different footprints from the library
| Cleanup Graphics...
  |
  | Cleanup redundant items, etc.
| Cleanup Tracks & Vias...
  |
  | Cleanup redundant items, shorting items, etc.
| Edit Text & Graphics Properties...
  |
  | Edit Text and graphics properties globally across board
| Edit Track & Via Properties...
  |
  | Edit track and via properties globally across board
| Global Deletions...
  |
  | Delete tracks, footprints and graphic items from board
| Remove Unused Pads...
  |
  | Remove or restore the unconnected inner layers on through hole pads and vias
| Swap Layers...
  |
  | Move tracks or drawings from one layer to another
| Update Footprint…
  |
  | Update footprint to include any changes from the library
| Update Footprints from Library...
  |
  | Update footprints to include any changes from the library
| Clearance Resolution...
  |
  | Show clearance resolution for the active layer between two selected objects
| Constraints Resolution...
  |
  | Show constraints resolution for the selected object
| Show Board Statistics
  |
  | Shows board statistics
| Add Aligned Dimension
  | kbd:[Ctrl+Shift+H]
  | Add an aligned linear dimension
| Draw Arc
  | kbd:[Ctrl+Shift+A]
  | Draw an arc
| Switch Arc Posture
  | kbd:[/]
  | Switch the arc posture
| Add Center Dimension
  |
  | Add a center dimension
| Draw Circle
  | kbd:[Ctrl+Shift+C]
  | Draw a circle
| Close Outline
  |
  | Close the in progress outline
| Decrease Line Width
  | kbd:[Ctrl+-]
  | Decrease the line width
| Delete Last Point
  | kbd:[Back]
  | Delete the last point added to the current item
| Draw Graphic Polygon
  | kbd:[Ctrl+Shift+P]
  | Draw a graphic polygon
| Increase Line Width
  | kbd:[Ctrl++]
  | Increase the line width
| Add Leader
  |
  | Add a leader dimension
| Draw Line
  | kbd:[Ctrl+Shift+L]
  | Draw a line
| Limit Lines to 45 deg
  |
  | Limit graphic lines to H, V and 45 degrees
| Add Orthogonal Dimension
  |
  | Add an orthogonal dimension
| Add Board Characteristics
  |
  | Add a board characteristics table on a graphic layer
| Import Graphics...
  | kbd:[Ctrl+Shift+F]
  | Import 2D drawing file
| Add Stackup Table
  |
  | Add a board stackup table on a graphic layer
| Draw Rectangle
  |
  | Draw a rectangle
| Add Rule Area
  | kbd:[Ctrl+Shift+K]
  | Add a rule area (keepout)
| Place the Footprint Anchor
  | kbd:[Ctrl+Shift+N]
  | Set the coordinate origin point (anchor) of the footprint
| Add a Similar Zone
  | kbd:[Ctrl+Shift+.]
  | Add a zone with the same settings as an existing zone
| Add Text
  | kbd:[Ctrl+Shift+T]
  | Add a text item
| Add Vias
  | kbd:[Ctrl+Shift+V]
  | Add free-standing vias
| Add Filled Zone
  | kbd:[Ctrl+Shift+Z]
  | Add a filled zone
| Add a Zone Cutout
  | kbd:[Shift+C]
  | Add a cutout area of an existing zone
| Get and Move Footprint
  | kbd:[T]
  | Selects a footprint by reference designator and places it under the cursor for moving
| Change Track Width
  |
  | Updates selected track & via sizes
| Create Array…
  | kbd:[Ctrl+T]
  | Create array
| Delete Full Track
  | kbd:[Shift+Del]
  | Deletes selected item(s) and copper connections
| Duplicate and Increment
  | kbd:[Ctrl+Shift+D]
  | Duplicates the selected item(s), incrementing pad numbers
| Fillet Tracks
  |
  | Adds arcs tangent to the selected straight track segments
| Change Side / Flip
  | kbd:[F]
  | Flips selected item(s) to opposite side of board
| Mirror
  |
  | Mirrors selected item
| Move Exactly…
  | kbd:[Ctrl+M]
  | Moves the selected item(s) by an exact amount
| Properties…
  | kbd:[E]
  | Displays item properties dialog
| Rotate Counterclockwise
  | kbd:[R]
  | Rotates selected item(s) counterclockwise
| Rotate Clockwise
  | kbd:[Shift+R]
  | Rotates selected item(s) clockwise
| Copy with Reference
  |
  | Copy selected item(s) to clipboard with a specified starting point
| Move
  | kbd:[M]
  | Moves the selected item(s)
| Move with Reference
  |
  | Moves the selected item(s) with a specified starting point
| Auto-finish Track
  | kbd:[F]
  | Automatically finishes laying the current track.
| Break Track
  |
  | Splits the track segment into two segments connected at the cursor position.
| Custom Track/Via Size…
  | kbd:[Q]
  | Shows a dialog for changing the track width and via size.
| Route Differential Pair
  | kbd:[6]
  | Route differential pairs
| Differential Pair Dimensions...
  |
  | Open Differential Pair Dimension settings
| Drag (45 degree mode)
  | kbd:[D]
  | Drags the track segment while keeping connected tracks at 45 degrees.
| Drag (free angle)
  | kbd:[G]
  | Drags the nearest joint in the track without restricting the track angle.
| Finish Track
  | kbd:[End]
  | Stops laying the current track.
| Router Highlight Mode
  |
  | Switch router to highlight mode
| Place Blind/Buried Via
  | kbd:[Alt+Shift+V]
  | Adds a blind or buried via at the end of currently routed track.
| Place Microvia
  | kbd:[Ctrl+V]
  | Adds a microvia at the end of currently routed track.
| Place Through Via
  | kbd:[V]
  | Adds a through-hole via at the end of currently routed track.
| Select Layer and Place Blind/Buried Via…
  | kbd:[Alt+<]
  | Select a layer, then add a blind or buried via at the end of currently routed track.
| Select Layer and Place Through Via…
  | kbd:[<]
  | Select a layer, then add a through-hole via at the end of currently routed track.
| Set Layer Pair...
  |
  | Change active layer pair for routing
| Interactive Router Settings…
  | kbd:[Ctrl+Shift+,]
  | Open Interactive Router settings
| Router Shove Mode
  |
  | Switch router to shove mode
| Route Single Track
  | kbd:[X]
  | Route tracks
| Switch Track Posture
  | kbd:[/]
  | Switches posture of the currently routed track.
| Track Corner Mode
  | kbd:[Ctrl+/]
  | Switches between sharp and rounded corners when routing tracks.
| Undo last segment
  | kbd:[Back]
  | Stops laying the current track.
| Router Walkaround Mode
  |
  | Switch router to walkaround mode
| Deselect All Tracks in Net
  |
  | Deselects all tracks & vias belonging to the same net.
| Filter Selected Items...
  |
  | Remove items from the selection by type
| Select/Expand Connection
  | kbd:[U]
  | Selects a connection or expands an existing selection to junctions, pads, or entire connections
| Select All Tracks in Net
  |
  | Selects all tracks & vias belonging to the same net.
| Sheet
  |
  | Selects all footprints and tracks in the schematic sheet
| Items in Same Hierarchical Sheet
  |
  | Selects all footprints and tracks in the same schematic sheet
| Decrease Amplitude
  | kbd:[4]
  | Decrease meander amplitude by one step.
| Increase Amplitude
  | kbd:[3]
  | Increase meander amplitude by one step.
| End Track
  | kbd:[End]
  | Stops laying the current meander.
| Length Tuning Settings…
  | kbd:[Ctrl+L]
  | Sets the length tuning parameters for currently routed item.
| Decrease Spacing
  | kbd:[2]
  | Decrease meander spacing by one step.
| Increase Spacing
  | kbd:[1]
  | Increase meander spacing by one step.
| New Track
  | kbd:[X]
  | Starts laying a new track.
| Tune length of a differential pair
  | kbd:[8]
  | Tune length of a differential pair
| Tune skew of a differential pair
  | kbd:[9]
  | Tune skew of a differential pair
| Tune length of a single track
  | kbd:[7]
  | Tune length of a single track
| Add Microwave Polygonal Shape
  |
  | Create a microwave polygonal shape from a list of vertices
| Add Microwave Gap
  |
  | Create gap of specified length for microwave applications
| Add Microwave Line
  |
  | Create line of specified length for microwave applications
| Add Microwave Stub
  |
  | Create stub of specified length for microwave applications
| Add Microwave Arc Stub
  |
  | Create stub (arc) of specified size for microwave applications
| Footprint Checker
  |
  | Show the footprint checker window
| Copy Footprint
  |
  | 
| Create Footprint...
  |
  | Create a new footprint using the Footprint Wizard
| Cut Footprint
  |
  | 
| Delete Footprint from Library
  |
  | 
| Edit Footprint
  |
  | Show selected footprint on editor canvas
| Export Footprint...
  |
  | Export footprint to file
| Footprint Properties...
  |
  | Edit footprint properties
| Hide Footprint Tree
  |
  | 
| Import Footprint...
  |
  | Import footprint from file
| New Footprint...
  | kbd:[Ctrl+N]
  | Create a new, empty footprint
| Paste Footprint
  |
  | 
| Repair Footprint
  |
  | Run various diagnostics and attempt to repair footprint
| Show Footprint Tree
  |
  | 
| Paste Default Pad Properties to Selected
  |
  | Replace the current pad's properties with those copied earlier
| Copy Pad Properties to Default
  |
  | Copy current pad's properties
| Push Pad Properties to Other Pads...
  |
  | Copy the current pad's properties to other pads
| Default Pad Properties…
  |
  | Edit the pad properties used when creating new pads
| Renumber Pads…
  |
  | Renumber pads by clicking on them in the desired order
| Edit Pad as Graphic Shapes
  | kbd:[Ctrl+E]
  | Ungroups a custom-shaped pad for editing as individual graphic shapes
| Add Pad
  |
  | Add a pad
| Finish Pad Edit
  | kbd:[Ctrl+E]
  | Regroups all touching graphic shapes into the edited pad
| Create Corner
  | kbd:[Ins]
  | Create a corner
| Remove Corner
  |
  | Remove corner
| Position Relative To…
  | kbd:[Shift+P]
  | Positions the selected item(s) by an exact amount relative to another
| Geographical Reannotate...
  |
  | Reannotate PCB in geographical order
| Refresh Plugins
  |
  | Reload all python plugins and refresh plugin menus
| Open Plugin Directory
  |
  | Opens the directory in the default system file manager
| Fill
  |
  | Fill zone(s)
| Fill All
  | kbd:[B]
  | Fill all zones
| Unfill
  |
  | Unfill zone(s)
| Unfill All
  | kbd:[Ctrl+B]
  | Unfill all zones
|===

=== 3D Viewer

[width="100%",options="header",cols="25%,15%,60%"]
|===
| Action | Default Hotkey | Description
| Add Floor
  |
  | Adds a floor plane below the board (slow)
| Anti-aliasing
  |
  | Render with improved quality on final render (slow)
| Toggle SMD 3D models
  | kbd:[S]
  | Toggle 3D models with 'Surface mount' attribute
| Toggle Through Hole 3D models
  | kbd:[T]
  | Toggle 3D models with 'Through hole' attribute
| Toggle Virtual 3D models
  | kbd:[V]
  | Toggle 3D models with 'Virtual' attribute
| Flip Board
  |
  | Flip the board view
| Home view
  | kbd:[Home]
  | Home view
| CAD Color Style
  |
  | Use a CAD color style based on the diffuse color of the material
| Use Diffuse Only
  |
  | Use only the diffuse color property from model 3D model file
| Use All Properties
  |
  | Use all material properties from each 3D model file
| Move board Down
  | kbd:[Down]
  | Move board Down
| Move board Left
  | kbd:[Left]
  | Move board Left
| Move board Right
  | kbd:[Right]
  | Move board Right
| Move board Up
  | kbd:[Up]
  | Move board Up
| No 3D Grid
  |
  | No 3D Grid
| Center pivot rotation
  | kbd:[Space]
  | Center pivot rotation (middle mouse click)
| Post-processing
  |
  | Apply Screen Space Ambient Occlusion and Global Illumination reflections on final render (slow)
| Procedural Textures
  |
  | Apply procedural textures to materials (slow)
| Render Shadows
  |
  | Render Shadows
| Reset view
  | kbd:[R]
  | Reset view
| Rotate 45 degrees over Z axis
  | kbd:[Tab]
  | 
| Rotate X Clockwise
  |
  | Rotate X Clockwise
| Rotate X Counterclockwise
  |
  | Rotate X Counterclockwise
| Rotate Y Clockwise
  |
  | Rotate Y Clockwise
| Rotate Y Counterclockwise
  |
  | Rotate Y Counterclockwise
| Rotate Z Clockwise
  |
  | Rotate Z Clockwise
| Rotate Z Counterclockwise
  |
  | Rotate Z Counterclockwise
| 3D Grid 1mm
  |
  | 3D Grid 1mm
| 3D Grid 2.5mm
  |
  | 3D Grid 2.5mm
| 3D Grid 5mm
  |
  | 3D Grid 5mm
| 3D Grid 10mm
  |
  | 3D Grid 10mm
| Show 3D Axis
  |
  | Show 3D Axis
| Show Model Bounding Boxes
  |
  | Show Model Bounding Boxes
| Reflections
  |
  | Render materials with reflective properties on final render (slow)
| Refractions
  |
  | Render materials with refractive properties on final render (slow)
| Toggle adhesive display
  |
  | Toggle display of adhesive layers
| Toggle board body display
  |
  | Toggle board body display
| Toggle comments display
  |
  | Toggle display of comments and drawings layers
| Toggle ECO display
  |
  | Toggle display of ECO layers
| Toggle orthographic projection
  |
  | Enable/disable orthographic projection
| Toggle realistic mode
  |
  | Toggle realistic mode
| Toggle silkscreen display
  |
  | Toggle display of silkscreen layers
| Toggle solder mask display
  |
  | Toggle display of solder mask layers
| Toggle solder paste display
  |
  | Toggle display of solder paste layers
| Toggle zone display
  |
  | Toggle zone display
| View Back
  | kbd:[Shift+Y]
  | View Back
| View Bottom
  | kbd:[Shift+Z]
  | View Bottom
| View Front
  | kbd:[Y]
  | View Front
| View Left
  | kbd:[Shift+X]
  | View Left
| View Right
  | kbd:[X]
  | View Right
| View Top
  | kbd:[Z]
  | View Top
|===
